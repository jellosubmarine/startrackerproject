#!/usr/bin/python
## @package check_match
#  This module is part of ESTCube-2 Star tracker pattern matching PC prototype.
#  It checks if picture stars matched with match.py are in corr.fits file returned by Astrometry.net
#  Inputs:
#  log/match.csv - match to check
#  data/bsc_m4_r270-350.csv - from bright star catalog we get coordinates of matched stars
#  data/deneb.fits - corr.fits file with actually star coordinates
#
#  More details.

import csv
import math
from astropy.io import fits

## Read in match file. We are only interested in mathced stars bright star catalog IDs.
#  Method returns list of list of four ints
#  Example [7665,8009,7726,7854]
#  @fileName
def readInMatch(fileName):
    with open(fileName, 'rb') as matchFile:
        match = csv.reader(matchFile, delimiter=',', quotechar='"')
        headers = match.next()
        i_A = headers.index('A_db')
        i_B = headers.index('B_db')
        i_C = headers.index('C_db')
        i_D = headers.index('D_db')

        quads = []
        for row in match:
            quads.append([int(row[i_A]), int(row[i_B]), int(row[i_C]), int(row[i_D])])
        return quads

## Read in bright star catalog file. We are only interested in ID, RA_D and DEC_D columns.
#  Method returns dictionary with bsc ID as key, tuple (RA_D, DEC_D) as value
#  @fileName
def readInBSC(fileName):
    with open(fileName, 'rb') as bscFile:
        bsc = csv.reader(bscFile, delimiter=',', quotechar='"')
        headers = bsc.next()
        i_id = headers.index('ID')
        i_ra = headers.index('RA_D')
        i_dec = headers.index('DEC_D')

        stars = {}
        for row in bsc:
            stars[int(row[i_id])] = (float(row[i_ra]), float(row[i_dec]))
        return stars

## Read in index ra and dec from detected_stars.fits returned from Astrometry.net
#  Method returns list of list of 2 floats
#  @fileName
def readCorr(fileName):
    table = fits.open(fileName, format='ascii')
    stars = []
    for row in table[1].data:
        stars.append([float(row['ra']), float(row['dec'])])
    return stars

## Main method for testing match.csv to corr.fits
def main():

    # 1. Read in match.csv"
    quads = readInMatch('log/match.csv')

    # 2. Read in brightstarcatalog.csv"
    bscDict = readInBSC('data/brightstarcatalog.csv')

    # 3. Read in detected_stars.fits"
    corrFits = readCorr('data/deneb_detect_ra_dec.fits')

    # 4. match_corr = join match with bsc

    # 5. search corr for match_corr
    result = []
    threshold = 1
    for q in quads:
        matched = 0
        row = ""
        for s in q:
            for c in corrFits:
                if math.fabs(c[0] - bscDict[s][0]) < threshold \
                    and math.fabs(c[1] - bscDict[s][1]) < threshold :
                    matched += 1
                    row += "ID:%d RA:%.2f DEC:%.2f == RA:%.2f DEC:%.2f \n" % (s, bscDict[s][0], bscDict[s][1], c[0], c[1])
                    break
        if matched > 3 : result.append(row)

    for r in result:
        print r + '\n'

if __name__ == "__main__":
    main()
