/*
# Thanks to Astrometry.net suite
*/
#pragma once
#ifndef STAR_UTILITY_H
#define STAR_UTILITY_H

// upper bound of dimquads value
#define DQMAX 5

class StarUtility
{
public:
	StarUtility();
	~StarUtility();
	
public:
	static bool starCoordinate(const double *s, const double *r, double *x, double *y, bool tangent);
	static void starMidpoint(double* midpoint, const double* pos_a, const double* pos_b);
};

#endif /* !STAR_UTILITY_H */
