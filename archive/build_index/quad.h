/*
# Thanks to Astrometry.net suite
*/
#pragma once
#ifndef STAR_QUAD_H
#define STAR_QUAD_H

class Quad
{
public:
	Quad();
	~Quad();

private:
	int dim_quads_;
	int dim_codes_;
	int id_;

	int star_a_;

	char *quad_fn_;
	char *code_fn_;
	char *skdt_fn_;

	double quad_d2_lower_;
	double quad_d2_upper_;
	bool use_d2_lower_;
	bool use_d2_upper_;

private:
	friend class QuadBuilder;
};


#endif /* !STAR_QUAD_H */
