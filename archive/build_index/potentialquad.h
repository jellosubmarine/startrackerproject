/*
# Thanks to Astrometry.net suite
*/
#pragma once
#ifndef POTENTIAL_QUAD_H
#define POTENTIAL_QUAD_H

class PotentialQuad
{
public:
	PotentialQuad();
	~PotentialQuad();
	
	PotentialQuad& operator=(const PotentialQuad &other);

private:
	static const int SIZE = 3;
	
	double* mid_ab_;
	double pos_x_;
	double pos_y_;
	double cos_theta_;
	double sin_theta_;
	
	int star_a_;
	int star_b_;
	int star_id_a_;
	int star_id_b_;
	int* inside_box_;
	int outside_box_;
	
	bool scale_ok_;
	bool check_ok_;

private:
	friend class QuadBuilder;
};

#endif /* !POTENTIAL_QUAD_H */