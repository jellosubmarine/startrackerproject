#include "stdafx.h"  
#include "quadbuilder.h"
#include "an-bool.h"
#include <assert.h>
#include <new>

QuadBuilder::QuadBuilder()
{
	
}

QuadBuilder::~QuadBuilder()
{
	delete[] inside_box_;
	delete[] pquad_;
}

bool QuadBuilder::checkStarAB(QuadBuilder* qb, PotentialQuad* pq, Quad* token) {
	return (pq->star_a_ == token->star_a_);
}

int QuadBuilder::checkInternalStars(QuadBuilder * qb, int sA, int sB, int * sC, int NC, void * token)
{
	// not implemented
	return 0;
}

bool QuadBuilder::isPartialQuad(QuadBuilder* qb, unsigned int* quad, int nstars, void* token) {
	// not implemented
	return false;
}

void QuadBuilder::addQuad(QuadBuilder* qb, unsigned int* stars, void* token) {
	// not implemented
	/*int i;
	hpquads_t* me = vtoken;
	bl_append(me->quadlist, stars);
	if (me->count_uses) {
		for (i = 0; i<me->dimquads; i++)
			me->nuses[stars[i]]++;
	}
	qb->stop_creating = TRUE;
	me->quad_created = TRUE;*/
}

bool QuadBuilder::isQuadFull(QuadBuilder* qb, unsigned int* quad, int nstars, void* token) {
	// not implemented
	/*hpquads_t* me = vtoken;
	anbool dup;
	if (!me->bigquadlist)
		return TRUE;
	dup = bt_contains2(me->bigquadlist, quad, compare_quads, &me->dimquads);
	return !dup;*/
	return false;
}

int QuadBuilder::createQuadBuilder(QuadBuilder *qb)
{
	PotentialQuad *qb_pquad;
	StarQuad quad;

	int star_a = 0, star_b, star_c, star_d, new_star;
	int outside_box;
	
	// ensure the arrays are large enough...
	if (qb->star_count_ > qb->n_cq_) {
		delete[] qb->inside_box_;
		delete[] qb->pquad_;
		
		qb->n_cq_ = qb->star_count_;
		qb->inside_box_ = new(std::nothrow) int[qb->star_count_];
		qb->pquad_ = new(std::nothrow) PotentialQuad[qb->star_count_ * qb->star_count_];
		
		if (qb->inside_box_ == nullptr || qb->pquad_ == nullptr) {
			//log error
			return -1;
		}
	}

	qb_pquad = qb->pquad_;

	/*
	Each time through the "for" loop below, we consider a new star ("newpoint").  
	First, we try building all quads that have the new star on the diagonal (star B).  
	Then, we try building all quads that have the star not on the diagonal (star D).
	Note that we keep the invariants iA < iB and iC < iD.
	*/
	memset(&quad, 0, sizeof(StarQuad));
	for (new_star = 0; new_star < qb->star_count_ && !qb->stop_creating_; new_star++) {
		PotentialQuad* pq;
		
		//log("Adding new star %i\n", newpoint);
		// quads with the new star on the diagonal:
		star_b = new_star;
		for (star_a = 0; star_a < new_star && !qb->stop_creating_; star_a++) {
			pq = qb_pquad + star_a * qb->star_count_ + star_b;
			pq->inside_box_ = NULL;
			pq->outside_box_ = 0;
			pq->star_a_ = star_a;
			pq->star_b_ = star_b;

			checkScale(qb, pq);
			if (!pq->scale_ok_) {
				//log("Dropping pair %i, %i based on scale\n", newpoint, iA);
				continue;
			}

			quad.star[0] = pq->star_id_a_;
			quad.star[1] = pq->star_id_b_;

			pq->check_ok_ = TRUE;

			if (qb->ab_stars_token_ != nullptr) {
				pq->check_ok_ = qb->checkStarAB(qb, pq, qb->ab_stars_token_);
			}
				
			if (!pq->check_ok_) {
				//logv("Failed check for AB stars\n");
				continue;
			}

			// list the possible internal stars...
			outside_box = 0;
			for (star_c = 0; star_c < new_star; star_c++) {
				if ((star_c == star_a) || (star_c == star_b)) {
					continue;
				}
					
				qb->inside_box_[outside_box] = star_c;
				outside_box++;
			}

			//log("Number of possible internal stars for pair %i, %i: %i\n", newpoint, iA, ninbox);

			// check which ones are inside the box...
			outside_box = checkInbox(pq, qb->inside_box_, outside_box, qb->star_xyz_);
			//log("Number of stars in the box: %i\n", ninbox);

			if (outside_box && qb->internal_stars_token_ != nullptr) {
				outside_box = qb->checkInternalStars(qb, quad.star[0], quad.star[1], qb->inside_box_,
					outside_box, qb->internal_stars_token_);
			}
			//log("Number of stars in the box after checking: %i\n", ninbox);

			addInteriorStars(qb, outside_box, qb->inside_box_, &quad, 2, qb->dim_quads_, 0);
			if (qb->stop_creating_) {
				continue;
			}
				
			pq->inside_box_ = new(std::nothrow) int[qb->star_count_];
			if (pq->inside_box_ == nullptr) {
				//log error
				exit(-1);
			}

			pq->outside_box_ = outside_box;
			memcpy(pq->inside_box_, qb->inside_box_, outside_box * sizeof(int));
			//log("iA=%i, iB=%i: saved %i 'inbox' entries.\n", iA, iB, ninbox);
		}
		
		int star_a_dup = star_a;

		// quads with the new star not on the diagonal:
		star_d = new_star;
		for (star_a = 0; star_a < new_star && !qb->stop_creating_; star_a++) {
			for (star_b = star_a + 1; star_b < new_star; star_b++) {
				pq = qb_pquad + star_a*qb->star_count_ + star_b;
				
				if (!(pq->scale_ok_ && pq->check_ok_)) {
					continue;
				}
					
				// check if this new star is in the box.
				qb->inside_box_[0] = star_d;
				outside_box = checkInbox(pq, qb->inside_box_, 1, qb->star_xyz_);
				if (!outside_box) {
					continue;
				}
					
				if (qb->internal_stars_token_ != nullptr) {
					outside_box = qb->checkInternalStars(qb, quad.star[0], quad.star[1],
						qb->inside_box_, outside_box, qb->internal_stars_token_);
				}
					
				if (!outside_box) {
					continue;
				}
					
				pq->inside_box_[pq->outside_box_] = star_d;
				pq->outside_box_++;

				quad.star[0] = pq->star_id_a_;
				quad.star[1] = pq->star_id_b_;

				addInteriorStars(qb, pq->outside_box_, pq->inside_box_, &quad, 2, qb->dim_quads_, 0);
				if (qb->stop_creating_) {
					star_a = star_a_dup;
					continue;
				}
			}
		}
	}

	for (int i = 0; i < qb->star_count_; i++) {
		int limit = (i == new_star) ? star_a : i;
		for (int j = 0; j < limit; j++) {
			PotentialQuad* pq = qb_pquad + j * qb->star_count_ + i;
			delete[] pq->inside_box_;
		}
	}
	return 0;
}

void QuadBuilder::checkScale(QuadBuilder * qb, PotentialQuad * pq)
{
	double *pos_a, *pos_b;
	double b_x = 0, b_y = 0;
	double a_bx, a_by;
	double inv_scale;
	double scale_base(0);
	anbool is_ok;

	if (!(qb->check_scale_low_ || qb->check_scale_high_)) {
		pq->scale_ok_ = TRUE;
		return;
	}
	pos_a = qb->star_xyz_ + pq->star_a_ * 3;
	pos_b = qb->star_xyz_ + pq->star_b_ * 3;
	
	pq->scale_ok_ = TRUE;
	if (qb->check_scale_low_ && scale_base < qb->quad_low_)
		pq->scale_ok_ = FALSE;

	if (pq->scale_ok_ && qb->check_scale_high_ && scale_base > qb->quad_high_)
		pq->scale_ok_ = FALSE;

	if (!pq->scale_ok_) {
		qb->bad_scale_count_++;
		return;
	}

	StarUtility::starMidpoint(pq->mid_ab_, pos_a, pos_a);
	pq->scale_ok_ = TRUE;
	pq->star_id_a_ = qb->star_inds_[pq->star_a_];
	pq->star_id_b_ = qb->star_inds_[pq->star_b_];
	
	is_ok = StarUtility::starCoordinate(pos_a, pq->mid_ab_, &pq->pos_y_, &pq->pos_x_, TRUE);
	assert(is_ok);
	is_ok = StarUtility::starCoordinate(pos_b, pq->mid_ab_, &b_y, &b_x, TRUE);
	assert(is_ok);
	
	a_bx = b_x - pq->pos_x_;
	a_by = b_y - pq->pos_y_;
	inv_scale = 1.0 / (a_bx * a_bx + a_by * a_by);
	pq->cos_theta_ = (a_by + a_bx) * inv_scale;
	pq->sin_theta_ = (a_by - a_bx) * inv_scale;
}

int QuadBuilder::checkInbox(PotentialQuad* pq, int* inds, int n_inds, double* stars)
{
	double* starpos;
	double d_x = 0, d_y = 0;
	double a_dx, a_dy;
	double x, y;
	double radius;
	int destind = 0;
	int ind;
	anbool ok;
	
	for (int i = 0; i < n_inds; i++) {
		
		
		ind = inds[i];
		starpos = stars + ind * 3;

		ok = StarUtility::starCoordinate(starpos, pq->mid_ab_, &d_y, &d_x, TRUE);
		if (!ok) {
			//log("star coords not ok\n");
			continue;
		}

		a_dx = d_x - pq->pos_x_;
		a_dy = d_y - pq->pos_y_;
		x = a_dx * pq->cos_theta_ + a_dy * pq->sin_theta_;
		y = -a_dx * pq->sin_theta_ + a_dy * pq->cos_theta_;
		
		// make sure it's in the circle centered at (0.5, 0.5)...
		radius = (x * x - x) + (y * y - y);
		if (radius > 0.0) {
			//log("star not in circle\n");
			continue;
		}
		
		inds[destind] = ind;
		destind++;
	}
	return destind;
}

void QuadBuilder::addInteriorStars(QuadBuilder* qb, int ninbox, int* inbox, StarQuad* q,
	int starnum, int dimquads, int beginning)
{
	for (int i = beginning; i<ninbox; i++) {
		int iC = inbox[i];
		q->star[starnum] = qb->star_inds_[iC];
		
		if (starnum == dimquads - 1) {
			if (qb->full_quad_token_ != nullptr &&
				qb->isQuadFull(qb, q->star, dimquads, qb->full_quad_token_)) {
				continue;
			}

			qb->addQuad(qb, q->star, qb->add_quad_token);
		}
		else {
			if (qb->partial_quad_token_ != nullptr &&
				!qb->isPartialQuad(qb, q->star, starnum + 1, qb->partial_quad_token_)) {
				continue;
			}
				
			addInteriorStars(qb, ninbox, inbox, q, starnum + 1, dimquads, i + 1);
		}

		if (qb->stop_creating_) {
			return;
		}
	}
}