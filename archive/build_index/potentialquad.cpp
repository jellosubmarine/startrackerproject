#include "stdafx.h"   
#include "potentialquad.h"

PotentialQuad::PotentialQuad()
{
	mid_ab_ = new double[SIZE];
}

PotentialQuad::~PotentialQuad()
{
	delete[] mid_ab_;
}

PotentialQuad& PotentialQuad::operator=(const PotentialQuad & other)
{
	memcpy(mid_ab_, other.mid_ab_, SIZE * sizeof(double));
	pos_x_ = other.pos_x_;
	pos_y_ = other.pos_y_;
	cos_theta_ = other.cos_theta_;
	sin_theta_ = other.sin_theta_;
	star_a_ = other.star_a_;
	star_b_ = other.star_b_;
	star_id_a_ = other.star_id_a_;
	star_id_b_ = other.star_id_b_;
	inside_box_ = other.inside_box_;
	outside_box_ = other.outside_box_;
	scale_ok_ = other.scale_ok_;
	check_ok_ = other.check_ok_;

	return *this;
}
