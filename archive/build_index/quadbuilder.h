/*
# Thanks to Astrometry.net suite
*/
#pragma once
#ifndef QUAD_BUILDER_H
#define QUAD_BUILDER_H

#include "potentialquad.h"
#include "quad.h"
#include "starutility.h"

struct StarQuad {
	unsigned int star[DQMAX];
};

class QuadBuilder
{
public:
	QuadBuilder();
	~QuadBuilder();

private:
	double quad_low_;
	double quad_high_;
	double* star_xyz_;
	
	int* star_inds_;
	int* inside_box_;
	int star_count_;
	int dim_quads_;
	int n_cq_;
	int bad_scale_count_;
	
	bool check_scale_low_;
	bool check_scale_high_;
	bool stop_creating_;
	
	PotentialQuad* pquad_;
	Quad* ab_stars_token_;
	void* internal_stars_token_;
	Quad* partial_quad_token_;
	void* full_quad_token_;
	void* add_quad_token;

private:
	int createQuadBuilder(QuadBuilder* qb);
	static void checkScale(QuadBuilder* qb, PotentialQuad* pq);
	static int checkInbox(PotentialQuad* pq, int* inds, int n_inds, double* stars);
	static void addInteriorStars(QuadBuilder* qb, int ninbox, int* inbox, StarQuad* q,
		int starnum, int dimquads, int beginning);

	bool checkStarAB(QuadBuilder* qb, PotentialQuad* pq, Quad* token);
	int checkInternalStars (QuadBuilder* qb, int sA, int sB, int* sC, int NC, void* token);
	bool isPartialQuad (QuadBuilder* qb, unsigned int* quad, int nstars, void* token);
	static bool isQuadFull(QuadBuilder* qb, unsigned int* quad, int nstars, void* token);
	static void addQuad(QuadBuilder* qb, unsigned int* stars, void* token);
	
};

#endif /* !QUAD_BUILDER_H */
