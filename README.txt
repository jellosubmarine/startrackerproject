README

Purpose of this pattern matching algorithm(s) script is to implement few
(ex. Geometric hash code, Pyramid, Optimisti pattern matching) that are used on star trackers.
Modify them to be more suitable for FPGA usage. Meaning: Is it possible to make
computations parallel? Also try to use simple computations. Try to avoid division,
taking sqrt. Integrating etc.
Compare which one is fastest and has small enough database to fit into our memory.

Check electronics constraints in https://confluence.tudengisatelliit.ut.ee:8433/display/EC2ST/Hardware+design

Architecture
* createDBIndex.py creates DB of geometrics from Brightstarcatalog (output geometric hash code hash, star ID’s in Brightstarcatalog)
* match.py reads in pixel coordinates of dettected stars and DB created by previous file.
Finds matching quads.
