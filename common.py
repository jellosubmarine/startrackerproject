## @package Common
#  Package common is used to gather all general non-algorithm specific functions
#
#  More details.

import math

## Prints all items in list into file
#  @param list List which members will be printed
#  @fileName File name into which the list will be printed
def logList(list, fileName):
    file = open(fileName, 'w')
    for item in list:
        print >> file, str(item)

## Calculates star's Vega magnitude from it's flux. Flux of Vega is hard-coded.
#  Changes it if stra tracker hardware changes.
#
# @param FLUX flux of the star
def calculateMagnitude(FLUX):
    # Magnitude calculation
    mx0 = 0 # apparent magnitude of Vega star
    fx0 = 14700 # calculated FLUX of Vega star from FLUX of matched star (ID = 8833 in BSC)
    # field_x, field_y, field_ra, field_dec, index_x, index_y, index_ra, index_dec, index_id, field_id, match_weight, FLUX, BACKGROUND'; format = '1E'; unit = 'unknown'
    # 2117.586669921875, 751.740478515625, 351.21206233826325, 62.281537371998212, 2117.8331448047484, 752.00414702420346, 351.20936553191962, 62.282836928295659, 30, 0, 0.99985232906694921, 147.79797, 58.602173
    mx = mx0 - 2.5*math.log((FLUX / fx0), 10)
    return mx
