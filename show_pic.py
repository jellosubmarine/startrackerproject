import matplotlib.pyplot as plt
from astropy.visualization import astropy_mpl_style
plt.style.use(astropy_mpl_style)

from astropy.utils.data import download_file
from astropy.io import fits

fileName = 'data/deneb.fits'

table = fits.open(fileName)

table.info()

image_data = table.getdata()

plt.figure()
plt.imshow(image_data, cmap='gray')
plt.colorbar()
