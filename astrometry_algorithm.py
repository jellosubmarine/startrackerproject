def get_healpix_sky(pixlen):
    """
    Creates a HEALPix grid for iterating over pixels.
    Returns an empty HEALPix map (??).
    
    Keyword arguments:
    pixlen -- length/width of one pixel in degrees
    """
    # TODO
    raise NotImplementedError("Needs to be implemented, see astropy!")
    return


def choose_stars(skygrid, starnum):
    """
    Creates a dictionary of brightest stars for every pixel in the sky.
    Returns a dictionary, with HEALPix pixels as arguments and a set of
    starnum stars for each pixel as values.
    
    Keyword arguments:
    skygrid -- HEALPix grid for iterating over pixels
    starnum -- the number of stars wanted for each pixel
    """
    raise NotImplementedError("Needs to be implemented, see astropy!")
    for pixel in skygrid:
        # TODO: find starnum brightest stars
        pass
    return {}


def build_new_quad(pixel, diameter, quads, stars_used, starmax, stardict):
    """
    Creates a new quad out of stars that meets the following conditions:
    * is not already in the set quads;
    * has a diameter (distance between the two most distant stars)
      less than diameter;
    * has its center (midpoint of the diameter line) lie within the specified pixel;
    * does not include stars that have been used more than starmax times
      according to the stars_used dictionary, unless evoking this condition
      would mean that no such quad can be found, in which case it can
      be ignored.
    Returns the hash of a new quad.
    
    Keyword arguments:
    pixel -- the HEALPix pixel inside which the center of the quad must lie
    diameter -- maximum allowed quad diameter in degrees (NOT pixlen*diameter)
    quads -- a set of hashes of the quads that have already been formed
    stars_used -- a dictionary describing how many times each star has been used
                  for forming a quad
    starmax -- the maximum number of times each star can be used in all quads
    stardict -- all stars that we can use for forming all quads in the whole sky
    """
    # Determine which adjacent pixels to look into for possible quad stars
    # This depends on diameter. Astrometry.net uses values 1 to sqrt(2)
    # for the diameter. 1 would mean that possible quad stars could be
    # a maximum of one pixel away (a pixel adjacent or diagonal), sqrt(2)
    # increases possibilities to stars two pixels away as well.
    pixels = find_appropriate_pixels(diameter, pixel)
    
    # Iterate over the appropriate pixels and find stars that are a maximum
    # of diameter away from the pixel for which we are building a quad.
    # We search for those stars from the pixels determined earlier, not
    # the whole sky.
    stars = find_close_stars(diameter, pixel, pixels)
    
    # Sort the stars found according to brightness
    brightlist = sort_brightness(stars)
    
    # Inspect all possible quads in the order brightness of stars,
    # trying to find a quad that matches the specification (see docstring)
    best_quad_with_popular_stars = None
    for s1_index in range(len(brightlist)):
        for s2_index in range(s1_index+1, len(brightlist)):
            for s3_index in range(s2_index+1, len(brightlist)):
                for s4_index in range(s3_index+1, len(brightlist)):
                    star1 = brightlist[s1_index]
                    star2 = brightlist[s2_index]
                    star3 = brightlist[s3_index]
                    star4 = brightlist[s4_index]
                    quad = create_quad(star1, star2, star3, star4)
                    
                    # Check whether the diameter of the quad is small enough
                    if quad_diameter(quad) > diameter:
                        continue
                    
                    # Check whether this quad is already in the set of quads
                    if quad in quads:
                        continue
                    
                    # Check whether the center of the quad lies within the pixel
                    # The center is defined as the midpoint of the diameter
                    if not inside_pixel(quad_midpoint(quad), pixel):
                        continue
                    
                    # This quad might work! Check if it uses some star
                    # that has been used too many times in the quads already
                    if stars_used[star1] >= starmax
                       or stars_used[star2] >= starmax
                       or stars_used[star3] >= starmax
                       or stars_used[star4] >= starmax:
                        # We will still save this quad for later
                        # in case we cannot find a better quad
                        best_quad_with_popular_stars = quad
                    else:
                        # This quad is good, time to return it
                        return quad
    
    # If we got here, this means that we could not find a quad that would
    # meet the conditions. Check if there is one that almost meets the
    # conditions, but uses stars that are too popular.
    if best_quad_with_popular_stars:
        return best_quad_with_popular_stars
    
    # If there really is no quad that we could find that would meet
    # the specified conditions, we have probably limited one of the parameters
    # too much. Raise an exception.
    # TODO: Find an appropriate exception!
    raise Exception("Could not find a quad that would meet the requirements! Perhaps change diameter or number of stars used to form quads.")


def astrometry_catalog_filter(pixlen = 5, starnum = 5, starmax = 4, rounds = 8, diameter = 1):
    """
    Returns a set of quadruplets of stars (quads) from the catalog of stars
    to be used for astrometric calibration on ESTCube 2/3. The algorithm used
    for identifying the quads is modelled after Astrometry.net's paper
    "Blind astrometric calibration of arbitrary astronomical images".
    Return value is a set of the hashes of quads.
    Read more at https://arxiv.org/pdf/0910.2233.pdf.
    
    Keyword arguments:
    pixlen -- length/width of one HEALPix pixel in the sky in degrees (default = 5)
    starnum -- number of bright stars to use for quad formations (default = 5)
    starman -- number of times each star can be used for forming quads,
               may be overridden by the algorithm (default = 4)
    rounds -- number of quads the algorithm finds for every pixel (default = 8)
    diameter -- maximum diameter of a quad, using pixlen as a unit
                (by default 1, that is, maximum diameter of a quad is 1*pixlen)
    """
    # Divide sky up to pixels of size pixlen x pixlen
    skygrid = get_healpix_sky(pixlen)
    
    # Choose starnum brightest stars from each pixel
    stardict = choose_stars(skygrid, starnum)
    
    # Build quads
    quads = set() # The set of all quads
    stars_used = {} # Keeps track of how many times a star has been used
    
    for i in range(rounds):
        for pixel in skygrid:
            # Finds a new quad the center of which is in the pixel,
            # the diameter of which does not exceed diameter*pixlen
            # and which does not overuse any stars, if possible
            new_quad = build_new_quad(pixel, diameter*pixlen, quads, stars_used, starmax, stardict)
            quads.add(new_quad)
            
            # Update stars_used appropriately
            for star in new_quad:
                stardict[star] += 1
    
    return quads
