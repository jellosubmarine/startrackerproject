#!/usr/bin/python
#This program converts pixel coordinates from .axy file to
#celestial coordinates for further analysis in TopCat


import sys
import	numpy as np
import matplotlib.pyplot as plt
from astropy import wcs
from astropy.io import fits
from astropy.table import Table


hdulist=fits.open(sys.argv[1]) #first argument is .axy file made without -t
h = fits.open(sys.argv[2]) #second argument is TAN-SIP wcs header


j = wcs.WCS(h[0].header)
ra, dec = j.all_pix2world(hdulist[1].data.X, hdulist[1].data.Y, 1)
#converting pixel coordinates into celestial

t = fits.BinTableHDU.from_columns(
     [fits.Column(name='ra', format='E', array=ra),
      fits.Column(name='dec', format='E', array=dec)])
t.writeto(sys.argv[3]) #third argument is a new filename used in Topcat
