## @package createDBIndex
#  This module is part of ESTCube-2 Star tracker pattern matching PC prototype.
#  It creates DB of known patterns from Bright Star Catalog for Geometric Hash Code.
#  Purpose of this module is to test and document different pattern matching algorithms
#  for star tracker
#  Usage: python createDBIndex.py ....
#
#  More details.
import csv
import math
import itertools
import copy
import argparse
import healpy as hp
import time
from star import *
from common import *
from quad import *

# FOV constants
# our optics can detect stars up to 6Mag
maxMagnitude = 4.0
arcsecPerPix = 28.5 #arcsec/pixel
radsPerPix = arcsecPerPix/(3600*180/math.pi)
fovInRads = 0.2269 #0.34906585 might be more correct

# HEALPix and related constants
nside = 16 # Makes one pixel's side be approximately 45*sqrt(2)/nside degrees
nstars = 5 # Takes nstar brightest stars for every pixel
npix = hp.nside2npix(nside)
maxStarUsage = 5 # Defines the maximum number of times each star can be used in a quad
amquadIterations = 3 # How many quads per pixel should be constructed in astrometryQuads

## Read in Bright Star Catalog .csv file. More precisely ID, DEC_R, RA_R, MAG columns.
#  Ra and dec in bsc have to be in radians.
#  Right now BSC file name is hard-coded.
#  Method return list of Star objects (look star.py)
def readCSV():
    with open('data/brightstarcatalog.csv', 'rb') as csvfile:
        starcatalog = csv.reader(csvfile, delimiter=',', quotechar='"')
        headers = starcatalog.next()
        i_id = headers.index('ID')
        i_dec = headers.index('DEC_R')
        i_ra = headers.index('RA_R')
        i_mag = headers.index('MAG')

        stars = []#*len(starcatalog)

        for row in starcatalog:
            stars.append(Star(int(row[i_id]), math.sin(float(row[i_dec])), \
                            math.cos(float(row[i_dec])) * math.sin(float(row[i_ra])), \
                            math.cos(float(row[i_dec])) * math.cos(float(row[i_ra])), \
                            float(row[i_ra]), float(row[i_dec]),
                            float(row[i_mag])))
        return stars

## Filter list of Star objects based on magnitude.
#  Right now the catalog is cut by magnitude on value of maxMagnitude variable.
#  It should however take only 4 or 5 brightest per FOV.
#  @param catlog list of Star objects to filter
def filterCatalog(catalog):
    # TODO Filtering star catalog should be done in with Astropy.
    # Maybe it should be moved into readCSV method.
    # Ask Tonis Eenmae
    relevant_stars = [ s for s in catalog if s.magnitude <= maxMagnitude]
    return relevant_stars

## Filter list of Star objects Astrometry.net style.
#  Takes nstars (global variable) brightest stars per HEALPix.
#  NSIDE for HEALPix has been specified in global variables.
#  @param catalog list of Star objects to filter
def filterAstrometry(catalog, nstars):
    # Sort stars in catalog by magnitude
    stars_sorted = sorted(catalog, key=lambda s: s.magnitude)

    # For every pixel, find nstars brightest stars in that pixel
    starmatrix = [ [] for pix in range(npix) ]
    stars_in_matrix = [0]*npix

    for s in stars_sorted:
        pix = hp.pixelfunc.ang2pix(nside, math.pi/2-s.dec, s.ra)
        if stars_in_matrix[pix] < nstars and s.magnitude <= maxMagnitude:
            starmatrix[pix].append(s)
            stars_in_matrix[pix] += 1

    return starmatrix, [ s for slist in starmatrix for s in slist ]

## Finds all stars in a specified pixel and pixels that are not further than depth pixels away
#  @param detectableStars list of detectable Star objects in the sky
#  @param ipix number of specified pixel (assuming ring ordering)
#  @param starmatrix a list of lists that organizes stars by pixel number
#  @param depth how many pixels away the furthest star to be returned can be
def findNearbyStars(detectableStars, ipix, starmatrix, depth):
    # Find neighboring pixel numbers corresponding to the depth
    nearest_pixels = set([ipix])
    for i in range(depth):
        new_pixels = []
        for pixel in nearest_pixels:
            new_pixels += [ p for p in list(hp.pixelfunc.get_all_neighbours(nside, pixel)) if p != -1]
        nearest_pixels = nearest_pixels.union(set(new_pixels))

    # Find all stars in the pixel itself and neighboring pixels
    nearest_stars = []
    for pixel in nearest_pixels:
        if pixel != -1: # pixel == -1 happens if a neighbor in some direction does not exist (see healpy docs)
            nearest_stars += [ star for star in starmatrix[pixel] if star ]

    return nearest_stars

## Forms all Quad objects that fit inside the field of view
#  Time complexity O(n^4), constant significantly smaller than for naive quad formation
#  @param detectableStars list of detectable Star objects in the sky
#  @param depth how many layers of neighbouring pixels to look into to find stars (default = 5)
#  @param starmatrix (optional) a list of lists that organizes stars by pixel number (will be generated if not supplied)
def fasterQuads(detectableStars, depth, starmatrix = []): # TODO: CALCULATE DEPTH FROM SIZE OF FOV AND NSIDE
    if len(starmatrix) == 0:
        starmatrix = [ [] for pix in range(npix) ]
        for star in detectableStars:
            if star:
                starmatrix[hp.pixelfunc.ang2pix(nside, math.pi/2-star.dec, star.ra)].append(copy.deepcopy(star))

    # Find nearby stars by looking into neighboring pixels
    nearby_stars = [ findNearbyStars(detectableStars, i, starmatrix, depth) for i in range(npix) ] # TODO: deepcopy is not needed here, right?
    print len(nearby_stars)
    # Form quads
    combinationsInFOV = set()
    for i in range(npix):
        newcombos = [tuple(sorted([combo[0], combo[1], combo[2], combo[3]])) for combo in itertools.combinations(nearby_stars[i], 4) if maxDist(combo) <= fovInRads]
        for combo in newcombos:
            combinationsInFOV.add(copy.deepcopy(combo))
    return sorted([ sorted(list(combo)) for combo in combinationsInFOV ])

## DEPRECATED: Converts cartesian coordinates to spherical coordinates on a unit sphere
#  @param (x,y,z) cartesian coordinates of a point
def car2sph((x,y,z)):
    return (math.acos(z), math.atan(float(y)/x)) # Use float so python2 would not freak out over integers

## DEPRECATED: Converts spherical coordinates (radius = 1) to cartesian coordinates
#  @param (theta,phi) spherical coordinates of a point on a unit sphere (radius = 1)
def sph2car((theta, phi)):
    return (math.sin(theta)*math.cos(phi), math.sin(theta)*math.sin(phi), math.cos(theta))

## Finds midpoint of a quad
#  Midpoint is defined as the midpoint between two furthest stars
#  Relies only on spherical coordinates for now (may recompute unnecessarily)
#  @param quad a Quad object of Stars for which the midpoint is being computed
def find_quad_midpoint(quad):
    A = quad.A
    B = quad.B
    sz, sy, sx = (A.z + B.z, A.y + B.y, A.x + B.x)
    sumlen = math.sqrt(sx*sx + sy*sy + sz*sz)
    unitvector = (sz/sumlen, sy/sumlen, sx/sumlen)
    return car2sph(unitvector)

## Forms quads of stars for every pixel in the sky based on Astrometry.net algorithm.
#  @param detectableStars list of stars detectable in the sky
#  @param depth how many layers of neighbouring pixels to look into to find stars
#  @param starmatrix (optional) a list of lists that organizes stars by pixel number (will be generated if not supplied)
def astrometryQuads(detectableStars, depth, starmatrix = []):
    if len(starmatrix) == 0:
        starmatrix = [ [] for pix in range(npix) ]
        for star in detectableStars:
            if star:
                starmatrix[hp.pixelfunc.ang2pix(nside, math.pi/2-star.dec, star.ra)].append(copy.deepcopy(star))

    # Find nearby stars by looking into neighboring pixels
    nearby_stars = [ findNearbyStars(detectableStars, i, starmatrix, depth)
                     for i in range(npix) ] # TODO: deepcopy is not needed here, right?
    stars_used = {}
    # Form quads
    combinationsInFOV = []
    combos_iterated = [0]*npix
    for m in range(amquadIterations):
        #~ print "doing iteration number", m
        for i in range(npix):
            #~ print "finding a quad for pixel", i
            suitable_quad = None
            good_enough_quad = None
            prev_iteration_counter = 0
            for combo in itertools.combinations(nearby_stars[i], 4):
                if prev_iteration_counter < combos_iterated[i]:
                    prev_iteration_counter += 1
                    continue
                prev_iteration_counter += 1
                combos_iterated[i] += 1
                # Find the quad's diameter and limit it to fovInRads
                if maxDist(combo) > fovInRads:
                    #~ print "maxdist too large"
                    continue
                # Form a quad
                quad = Quad(combo)
                # Find out whether the quad's midpoint is located in the pixel
                theta, phi = find_quad_midpoint(quad)
                if hp.pixelfunc.ang2pix(nside, theta, phi) != i:
                    #~ print "midpoint not in", i, "but instead in", hp.pixelfunc.ang2pix(nside, theta, phi)
                    continue
                # Find out how many times each star in the quad has been used already
                star_used_too_much = False
                for star in combo:
                    if star.id in stars_used and stars_used[star.id] >= maxStarUsage:
                        #~ print "a star has been used too much"
                        star_used_too_much = True
                if star_used_too_much and good_enough_quad == None:
                    good_enough_quad = combo
                else:
                    suitable_quad = combo
                    break
            if suitable_quad != None:
                combinationsInFOV.append(suitable_quad)
                #~ print "found a suitable quad", suitable_quad
            elif good_enough_quad != None:
                combinationsInFOV.append(good_enough_quad)
                #~ print "found a good enough quad", good_enough_quad
            else:
                # There were no quads with the midpoint in the specified pixel
                # The pixel size or star catalog may be too small
                pass
    return combinationsInFOV

## Write relevant Quad data to file.
#  Right now the data is hash (xC, yC, xD, yD) and quad stars' bright star catalog IDs
#  @param dbQuads list of Quad objects
def writeDBToFile(dbQuads):
    with open('log/dbIndex.csv', 'wb') as csvfile:
        csvwriter = csv.writer(csvfile, delimiter=',',
                                quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csvwriter.writerow(['xC', 'yC', 'xD', 'yD', 'id_A', 'id_B', 'id_C', 'id_D', 'AX', 'AY', 'rot'])
        for row in dbQuads:
            csvwriter.writerow(list(row.getHash()) + [row.A.id, row.B.id, row.C.id, row.D.id, row.rotateX, row.rotateY, row.rotation])

## main method to create db Index for pattern matching
def main(astrometry_stars = False, astrometry_quads = False, faster_quads = False, timed = True):
    # 0. Start timer if time measurement is turned on
    if timed:
        start_time = time.time()

    # 1. Read in CSV
    bscMap = readCSV()
    logList(bscMap, 'log/catalog/bscMap.txt')

    # 2. Filtering star catalog
    starmatrix = []
    if astrometry_stars:
        starmatrix, detectableStars = filterAstrometry(bscMap, nstars)
    else:
        detectableStars = filterCatalog(bscMap)
    logList(detectableStars, 'log/catalog/detectableStars.txt')

    # 3. Combine sets of 4 stars
    if astrometry_quads:
        #~ depth = int(math.ceil((fovInRads/(math.pi/nside/2**1.5)-0.5)/2))
        depth = 5 # TODO: find correct formula for depth
        combinationsInFOV = astrometryQuads(detectableStars, depth, starmatrix = starmatrix)
    elif faster_quads:
        #~ depth = int(math.ceil((fovInRads/(math.pi/nside/2**1.5)-0.5)/2))
        depth = 5 # TODO: find correct formula for depth
        combinationsInFOV = fasterQuads(detectableStars, depth, starmatrix = starmatrix)
    else:
        combinationsInFOV = [ copy.deepcopy(combo) for combo in itertools.combinations(detectableStars, 4)
                                    if maxDist(combo) <= fovInRads]
    logList(combinationsInFOV, 'log/catalog/combsInFOV.txt')

    # 4. Project star sets to plane????

    # 5. compose Quads for 4 star sets
    dbQuads = [Quad(list(combo)) for combo in combinationsInFOV]

    # 6. Rotate(shift) star sets so that star A would be in point (0,0,1)
    # Moved it inside quad init
    #logList(dbQuads, 'log/catalog/dbQuads.txt')

    # 7. Write quad data to file
    writeDBToFile(dbQuads)

    # 8. If we need to measure time, then report it
    if timed:
        fout = open('log/catalog/timeSpent.txt', 'w')
        print >> fout, ("Time spent was "+str((time.time() - start_time))+" seconds")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Choose filtering and quad formation methods')
    parser.add_argument('--astrometryfilter', required=False, action='store_true',
                        help='use Astrometry.net filtering style for filtering stars')
    parser.add_argument('--astrometryquads', required=False, action='store_true',
                        help='use Astrometry.net algorithm for quad formation')
    parser.add_argument('--fasterquads', required=False, action='store_true',
                        help='use a faster method of finding quads')
    parser.add_argument('--timed', required=False, action='store_true',
                        help='time how long database creation takes')

    args = parser.parse_args()

    main(args.astrometryfilter, args.astrometryquads, args.fasterquads, args.timed)
