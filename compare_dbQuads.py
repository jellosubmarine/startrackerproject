import argparse
import csv
import copy

## Reads in dbQuads file and converts it to list of star IDs
#  
def read_dbQuads(db):
    with open(db, 'r') as dbFile:
        fin = [ item.split() for item in dbFile.read().split('\n') if item ]
        quads = [ [int(item[0]), int(item[7]), int(item[14]), int(item[21])] for item in fin ]
        return quads
    
def read_dbIndex(db):
    with open(db, 'rb') as dbFile:
        dbReader  = csv.reader(dbFile, delimiter=',', quotechar='"')
        headers = dbReader.next()
        i_A = headers.index('id_A')
        i_B = headers.index('id_B')
        i_C = headers.index('id_C')
        i_D = headers.index('id_D')
        
        catalog = []
        id = 0

        for row in dbReader:
            catalog.append([int(row[i_A]),int(row[i_B]),int(row[i_C]),int(row[i_D])])
        return catalog

def read_db(db):
    if "dbQuads.txt" in db:
        return read_dbQuads(db)
    return read_dbIndex(db)

## Function that takes the file names of two databases to be compared
#  Reports any discrepancies between the quads
#  Should use argparse instead of sys.args, needs to be rewritten
def main(db1, db2):
    # Read in databases
    quads1 = read_db(db1)
    quads2 = read_db(db2)
    
    # Compare database lengths, warn if the lengths do not match
    if len(quads1) != len(quads2):
        print "does not match: lengths are different as they are", len(quads1), len(quads2)
    
    # Compare sets of stars to each other, warn if find any stars that do not match
    for quad in quads1:
        if quad not in quads2:
            print "quad", quad, "not in the second database"
    for quad in quads2:
        if quad not in quads1:
            print "quad", quad, "not in the first database"

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Compare two dbIndex / dbQuads files')
    parser.add_argument('db1', help='first database file to compare')
    parser.add_argument('db2', help='second database file to compare')
    args = parser.parse_args()
    main(args.db1, args.db2)
