from createDBIndex import *

fin = open("log/dbQuads.txt", 'r')
t = fin.read().split('\n')
fin.close()

csvfile = open('log/dbIndex.csv', 'wb')
csvwriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
csvwriter.writerow(['xC', 'yC', 'xD', 'yD', 'id_A', 'id_B', 'id_C', 'id_D'])

for quad in t:
    if quad:
        xC, yC, xD, yD = [ float(x) for x in quad.split('(')[1].split(')')[0].split(' ') ]
        words = quad.split(' ')
        idA, idB, idC, idD = int(words[0]), int(words[7]), int(words[14]), int(words[21])
        csvwriter.writerow([xC, yC, xD, yD, idA, idB, idC, idD])
        
csvfile.close()
