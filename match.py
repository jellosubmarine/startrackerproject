#!/usr/bin/python
## @package match
#  This module is part of ESTCube-2 Star tracker pattern matching PC prototype.
#  It takes stars with pixel coordinates detected from picture to creates quads to
#  match with database for Geometric Hash Code.
#  Purpose of this module is to test and document different pattern matching algorithms
#  for star tracker
#
#  More details.
import csv
import math
import itertools
from astropy.io import fits
import copy
from star import *
from common import *
from quad import *
import time

from operator import itemgetter
from scipy.spatial import cKDTree as kdt
from numpy import array

# global variables used for reading and using dbIndex.
i_xC = -1
i_yC = -1
i_xD = -1
i_yD = -1
i_A = -1
i_B = -1
i_C = -1
i_D = -1

## Checks if two Quads fit into threshold
#  @param fst First Quad to compare.
#  @param snd Second Quad to compare.
#  @threshold thresold of check.
def compareQuadWithHash(quad, hash, threshold):
    return math.fabs(quad.xC - hash[0]) < threshold and \
            math.fabs(quad.yC - hash[1]) < threshold and\
            math.fabs(quad.xD - hash[2]) < threshold and\
            math.fabs(quad.yD - hash[3]) < threshold

## Read in picture coordinates from
#  Either from corr.fits returned by astrometry.net
#  or .csv file. More precisely field_x, field_y, Magnitude columns.
#  Method returns list of Star objects (look star.py)
#
# @param fileName File which consists detected star positions on picture
def readPictureFile(fileName):
    stars = []

    id = 0
    if fileName.endswith('.fits') :
        table = fits.open(fileName, format='ascii')
        for row in table[1].data:
            stars.append(Star(id, float(row['X']), float(row['Y']), 0.0, 0.0, 0.0, float(calculateMagnitude(row['FLUX']))))
            id += 1
    else :
        with open(fileName, 'rb') as pixfile:
            pix = csv.reader(pixfile, delimiter=';', quotechar='"')
            headers = pix.next()
            i_x = headers.index('field_x')
            i_y = headers.index('field_y')
            i_M = headers.index('Magnitude')

            for row in pix:
                stars.append(Star(id, float(row[i_x]), float(row[i_y]), 0.0, 0.0, 0.0, float(row[i_M])))
                id += 1
    return stars

## Read in catalog from .csv file. log/dbIndex.csv corresponds to what main function in
#  module createDBIndex.py has created.
#  Right now it consists hash (xC, yC, xD, yD) and quad stars' bright star catalog IDs
#
# @param fileName File which consists DBIndex
def readCatalogCSV(fileName):
    with open(fileName, 'rb') as dbFile:

        dbReader  = csv.reader(dbFile, delimiter=',', quotechar='"')
        headers = dbReader.next()
        global i_xC, i_yC, i_xD, i_yD, i_A, i_B, i_C, i_D
        i_xC = headers.index('xC')
        i_yC = headers.index('yC')
        i_xD = headers.index('xD')
        i_yD = headers.index('yD')
        i_A = headers.index('id_A')
        i_B = headers.index('id_B')
        i_C = headers.index('id_C')
        i_D = headers.index('id_D')
        i_AX = headers.index('AX')
        i_AY = headers.index('AY')
        i_rot = headers.index('rot')

        catalog = []
        id = 0

        for row in dbReader:
            catalog.append(copy.deepcopy(map(float, row)))
        return catalog

## Finds whitch Quad objects match with hashes in dbIndex
#
#  @param quads list of Quad objects (look Quad.py) that will be searched.
#  @param dbIndex list of hash-s and star ID-s.
def matchPixWithDb(quads, dbIndex):
    match = []
    threshold = 0.035
    for q in quads:
        for d in dbIndex:
            if compareQuadWithHash(q, d, threshold):
                match.append((q,d))
                break
    return match

## Finds which Quad objects match with hashes in dbIndex using modified binary search
#
#  @param quads list of Quad objects (look Quad.py) that will be searched.
#  @param dbIndex list of hash-s and star ID-s.
def smarterMatchWithDB(quads, dbIndex):
    match = []
    threshold = 0.035
    secondary_threshold = 30

    for q in quads:
        left = 0
        right = len(dbIndex) - 1
        while(1):
            if left > right:
                for i in range(max(mid - secondary_threshold, 0), min(mid + secondary_threshold, len(dbIndex) - 1)):
                    if compareQuadWithHash(q, dbIndex[i], threshold):
                        match.append((q,dbIndex[i]))
                        break
                break
            mid = (left + right)/2
            #print str(q.xC) + ' ' + str(dbIndex[mid][0]) + '\n'
            #print str(left) + ' ' + str(right) + ' ' + str(mid) + '\n'
            if compareQuadWithHash(q, dbIndex[mid], threshold):
                match.append((q,dbIndex[mid]))
                #print str(q) + ' ' + str(dbIndex[mid]) + '\n'
                break
            elif dbIndex[mid][0] > q.xC:
                right = mid - 1
            elif dbIndex[mid][0] < q.xC:
                left = mid + 1
            else:
                for i in range(mid - 200, mid + 200):
                    if compareQuadWithHash(q, dbIndex[i], threshold):
                        match.append((q,dbIndex[i]))
                        break
                break

            #print str(left) + ' ' + str(right) + ' ' + str(mid) + '\n'
            #print str(q.xC) + ' ' + str(dbIndex[mid][0]) + '\n'

    #print match
    return match

## Write match data to file.
#
#  @param match list of tuples of Quad object and dbIndex.csv file rows
def writeMatchToFile(match):
    with open('log/match.csv', 'wb') as matchFile:
        matchWriter = csv.writer(matchFile, delimiter=',',
                                quotechar='"', quoting=csv.QUOTE_MINIMAL)
        matchWriter.writerow(['A_pix', 'B_pix', 'C_pix', 'D_pix', 'A_db', 'B_db', \
            'C_db', 'D_db', 'cX_pix', 'cY_pix', 'dX_pix', 'dY_pix', 'cX_db', 'cY_db', 'dX_db', 'dY_db'])
        for row in match:
            matchWriter.writerow([row[0].A.id, row[0].B.id, row[0].C.id, row[0].D.id, \
                        int(row[1][i_A]), int(row[1][i_B]), int(row[1][i_C]), int(row[1][i_D])] + \
                        list(row[0].getHash()) + [row[1][i_xC], row[1][i_yC], row[1][i_xD], row[1][i_yD]])

## Uses kd-tree to find nearest neighbours so it could make fewer combinations, uses scipy package
#
#  @param stars Takes in the list of chosen stars on the picture
def smarterCombinations(stars):
    combinations = []
    stardic = {(star.x, star.y):star for star in stars}
    starsxy = list(stardic.keys())

    #print starsxy
    #data, bruteforce threshold, compact_nodes, copy_data, balanced_tree
    tree = kdt(starsxy, 3, False, False, False)
    for i in starsxy:
        neighbours = []
        distance = 500
        while(len(neighbours) < 20):
            neighbours = tree.query_ball_point(i, distance)
            distance += 100

        temp = []
        for j in neighbours:
            temp.append(stardic[starsxy[j]])

        combinations.extend([tuple(sorted([combo[0], combo[1], combo[2], combo[3]])) for combo in itertools.combinations(temp, 4)])
    #print combinations
    combinations = list(set(combinations))
    print len(combinations)
    #print "ok"
    return combinations



## main method to match db Index with star coordinates from picture
def main():
    f = open("Times.txt", "w")

    for i in range(0,1):
        # 1. Read in picture"
        # Take all stars with smaller magnitude than 6 from deneb_axy as deneb_axy as ~1k stars
        picStars = readPictureFile('data/deneb_axy.fits')
        starData = [star for star in picStars if star.magnitude < 6]
        logList(starData, 'log/match/bscMap.txt')

        # 2. Read in DB index"
        dbIndex = readCatalogCSV('log/dbIndex.csv')
        dbIndex.sort(key=itemgetter(0), reverse=False)
        start = time.time()
        # 3. Combine sets of 4 stars"
        #combinations = smarterCombinations(starData)
        combinations = [copy.deepcopy(combo) for combo in itertools.combinations(starData, 4)]
        #print combinations
        logList(combinations, 'log/match/combinations.txt')


        end = time.time()

        start1 = time.time()
        # 4. Compose Quads for 4 star sets
        quads = [Quad(list(combo)) for combo in combinations]
        logList(quads, 'log/match/quads.txt')
        end1 = time.time()

        start2 = time.time()
        # 5. Match with DB"
        #match = matchPixWithDb(quads, dbIndex)
        match = smarterMatchWithDB(quads, dbIndex)
        end2 = time.time()

        writeMatchToFile(match)

        #print end-start
        print (str)(end-start) + '\t' + (str)(end1-start1) + '\t' + (str)(end2-start2) + '\n'
        f.write((str)(end-start) + '\t' + (str)(end1-start1) + '\t' + (str)(end2-start2) + '\n')
    # 6. Find attitude quaternion from match"

    f.close()

if __name__ == "__main__":
    main()
