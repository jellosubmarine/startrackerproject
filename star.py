## @package Star
#  Contains class Star and class Star specific functions
#

import math
import itertools

## Contains relevant data for representing star position and magnitude.
#  Also functions to manipulate this data
#
class Star:

    ## The default constructor.
    def __init__(self):
        self.id = None
        self.x = None
        self.y = None
        self.z = None
        self.ra = None
        self.dec = None
        self.magnitude = None

    ## The constructor.
    #  Note: for 3d cartesian coordinates, the x-axis points to dec=pi/2
    #  @param self The object pointer.
    #  @param star_id Id of star.
    #  @param x 2d or 3d cartesian coordinate for star.
    #  @param y 2d or 3d cartesian coordinate for star.
    #  @param z 3d cartesian coordinate for star.
    #  @param ra Right ascension of the star.
    #  @param dec Declination of the star.
    #  @param magnitude Magnitude of the star.
    def __init__(self, id, x, y, z, ra, dec, magnitude):
        self.id = id
        self.x = x
        self.y = y
        self.z = z
        self.ra = ra
        self.dec = dec
        self.magnitude = magnitude

    ## Operator ">" functonality. Compares magnitudes between two stars.
    #  @param self The object pointer.
    #  @param other The other Quad to compare.
    def __gt__(self, other):
        return self.id > other.id
        #return self.magnitude > other.magnitude or (self.magnitude == other.magnitude and self.id > other.id)

    ## Operator "=" functonality. Compares magnitudes between two stars.
    #  @param self The object pointer.
    #  @param other The other Quad to compare.
    def __eq__(self, other):
        return self.id == other.id
        
    def __hash__(self):
        return self.id

    ## String representation of Quad.
    #  @param self The object pointer.
    def __repr__(self):
        # return "%d %.2f %.2f .." % (self.id, self.x, self.y, self.z, self.ra, self.dec, self.magnitude)
        # return "{} {:.2f} {:.2f} {:.2f} {} {} {}".format(
        #     self.id, self.x, self.y, self.z, self.ra, self.dec, self.magnitude)
       return str(self.id) + " " + str(self.x) + " " + str(self.y) + " " +\
       str(self.z) + " " + str(self.ra) + " " + str(self.dec) + " " + str(self.magnitude)

    ## String representation of Quad.
    #  @param self The object pointer.
    def __str__(self):
        return str(self.id) + " " + str(self.x) + " " + str(self.y) + " " +\
        str(self.z) + " " + str(self.ra) + " " + str(self.dec) + " " + str(self.magnitude)

    ## Returns distance between two stars in X-Y-Z room.
    #  @param self The object pointer. The star to find the distance from.
    #  @param other The star to find distance to.
    def distance(self, other):
        x1 = self.x
        x2 = other.x
        y1 = self.y
        y2 = other.y
        z1 = self.z
        z2 = other.z
        return math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) + (z1 - z2) * (z1 - z2))

    ## Returns distance between two stars in X-Y plane.
    #  @param self The object pointer. The star to find the distance from.
    #  @param other The star to find distance to.
    def distance2d(self, other):
        x1 = self.x
        x2 = other.x
        y1 = self.y
        y2 = other.y
        return math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2))

    ## Rotates star around X-axis. Y,Z and ra elements of self object will be changed.
    #  @param self The object pointer. The star to rotate around X-axis.
    #  @param theta Angle of rotation of the star.
    def rotateAroundX(self, theta):
        self.y, self.z = math.cos(theta) * self.y - math.sin(theta) * self.z, \
                         math.sin(theta) * self.y + math.cos(theta) * self.z
        self.ra -= theta
        return self

    ## Rotates star around Y-axis. X,Z and dec elements of self object will be changed.
    #  @param self The object pointer. The star to rotate around Y-axis.
    #  @param phi Angle of rotation of the star.
    def rotateAroundY(self, phi):
        self.x, self.z = math.cos(phi) * self.x - math.sin(phi) * self.z, \
                         math.sin(phi) * self.x + math.cos(phi) * self.z
        self.dec -= phi
        return self

## Finds max distance of star pairs in list.
#  Composes all combinations with 2 elements taken from list.
#  Finds distances between these 2 stars for all combinations.
#  Finds and returns the max value of these distances.
#  Used by Geometric Hash Code algorithm
#  @param list
def maxDist(list):
    return max(map(lambda pair: pair[0].distance(pair[1]), itertools.combinations(list, 2)))
