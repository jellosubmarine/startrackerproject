## @package Quad
#  Contains class Quad and class Quad specific functions
#  List of variables
#    star objects - self.A ; self.B ; self.C ; self.D
#    hash         - self.xC ; self.yC ; self.xD ; self.yD
#    rotation     - self.rotateX ; self.rotateY ; self.rotation

import itertools,copy,math
from star import *

## Contains relevant data and calculations for creating Geometric Hash Code
#
class Quad:

    ## The default constructor.
    def __init__(self):
        self.A = None
        self.B = None
        self.C = None
        self.D = None

    ## The constructor.
    #  @param self The object pointer.
    #  @param lst List of stars. Should contain 4 stars exactly.
    #  Divides 4 stars into base stars and coordinate stars. Calculates hash
    #  and does additional ordering.
    def __init__(self, lst):

        # pairs = [list(pair) for pair in itertools.combinations(lst, 2)]
        # dists = [pair[0].distance(pair[1]) for pair in pairs]
        # max_dist = max(dists)
        # max_pair = pairs[dists.index(max_dist)]
        # other = list(set(lst) - set(max_pair))

        # self.A = max_pair[0]
        # self.B = max_pair[1]
        # self.C = others[0]
        # self.D = others[1]

        bases = [[0,1],[0,2],[0,3],[1,2],[1,3],[2,3]]
        others = [[2,3],[1,3],[1,2],[0,3],[0,2],[0,1]]
        dists = [lst[i[0]].distance(lst[i[1]]) for i in bases]
        max_dist = max(dists)
        i = dists.index(max_dist)

        self.A = lst[bases[i][0]]
        self.B = lst[bases[i][1]]
        self.C = lst[others[i][0]]
        self.D = lst[others[i][1]]

        self.rotation = math.atan2((self.A.y-self.B.y),(self.A.x-self.B.x))
        self.shift()
        self.hash()
    ## Operator ">" functonality.
    #  @param self The object pointer.
    #  @param other The other Quad to compare.
    #  TODO This funcinality was needed in Haskell implementation. Not used in Python
    def __gt__(self, other):
        return (self.A > other.A
               or (self.A == other.A and self.B > other.B)
               or (self.A == other.A and self.B == other.B and self.C > other.C)
               or (self.A == other.A and self.B == other.B and self.C == other.C and self.D > other.D))

    ## Operator "=" functonality.
    #  @param self The object pointer.
    #  @param other The other Quad to compare.
    #  TODO This funcinality was needed in Haskell implementation. Not used in Python
    def __eq__(self, other):
        return self.A == other.A and self.B == other.B and self.C == other.C and self.D == other.D

    ## String representation of Quad.
    #  @param self The object pointer.
    def __repr__(self):
        return str(self.A) + " " + str(self.B) + " " + str(self.C) + " " + str(self.D) + \
                " (" + str(self.xC) + " " + str(self.yC) + " " + str(self.xD) + " " + str(self.yD) + ") (" + str(self.rotateX) + " " + str(self.rotateY) + " " + str(self.rotation) + ")"

    ## String representation of Quad.
    #  @param self The object pointer.
    def __str__(self):
        return str(self.A) + " " + str(self.B) + " " + str(self.C) + " " + str(self.D) + \
                " (" + str(self.xC) + " " + str(self.yC) + " " + str(self.xD) + " " + str(self.yD) + ") (" + str(self.rotateX) + " " + str(self.rotateY) + " " + str(self.rotation) + ")"

    ## Calculates hash for current quad.
    #  @param self The object pointer.
    #  Calculates hash. If necessary then reorders A,B and/or C,D then calculates hash again.
    def hash(self):
        self.calculateHash()
        # If xc + xd > 1 then swap base stars
        if self.xC + self.xD > 1 :
            self.A, self.B = self.B, self.A
            self.calculateHash()
        # If xc > xd then swap C and D
        if self.xC > self.xD :
            self.C, self.D = self.D, self.C
            self.calculateHash()
        return self.xC, self.yC, self.xD, self.yD

    ## Return hash of quad
    #  @param self The object pointer.
    def getHash(self):
        return self.xC, self.yC, self.xD, self.yD

    ## Calculates scale for current quad.
    #  @param self The object pointer.
    #  Scale for quad is distance between base stars.
    def scale (self):
        return (math.sqrt(2)/2) * self.A.distance(self.B)

    def shift(self):
        self.rotateX = copy.deepcopy(self.A.ra)  #original A ra
        self.rotateY = copy.deepcopy(self.A.dec) #original A dec
        for st in [self.A,self.B,self.C,self.D]:
            st.rotateAroundX(self.rotateX).rotateAroundY(self.rotateY)
        return self

    # Rotation (if AB is (0;0)-(1;1))
    # cos(a) = sqrt(2)/2 * Ox + sqrt(2)/2 * Oy
    # sin(a) = -sqrt(2)/2 * Ox + sqrt(2)/2 * Oy

    ## Calculates sine of the rotation of AB from line (0;0)-(1;1).
    #  @param self The object pointer.
    # Rotation (if AB is (0;0)-(1;1))
    # cos(a) = sqrt(2)/2 * Ox + sqrt(2)/2 * Oy
    # sin(a) = -sqrt(2)/2 * Ox + sqrt(2)/2 * Oy
    def rotationSinA(self):
        return (math.sqrt(2)/2) * ( - (self.B.x - self.A.x) + (self.B.y - self.A.y)) / self.A.distance(self.B)

    ## Calculates cosine of the rotation of AB from line (0;0)-(1;1).
    #  @param self The object pointer.
    # Rotation (if AB is (0;0)-(1;1))
    # cos(a) = sqrt(2)/2 * Ox + sqrt(2)/2 * Oy
    # sin(a) = -sqrt(2)/2 * Ox + sqrt(2)/2 * Oy
    def rotationCosA(self):
        return (math.sqrt(2)/2) * ((self.B.x - self.A.x) + (self.B.y - self.A.y)) / self.A.distance(self.B)

    ## Calculates hash for Quad with current ordering.
    #  @param self The object pointer.
    def calculateHash(self):
        self.xC =   (self.rotationCosA() * (self.C.x - self.A.x) + self.rotationSinA() * (self.C.y - self.A.y)) / self.scale()
        self.yC = (- self.rotationSinA() * (self.C.x - self.A.x) + self.rotationCosA() * (self.C.y - self.A.y)) / self.scale()
        self.xD =   (self.rotationCosA() * (self.D.x - self.A.x) + self.rotationSinA() * (self.D.y - self.A.y)) / self.scale()
        self.yD = (- self.rotationSinA() * (self.D.x - self.A.x) + self.rotationCosA() * (self.D.y - self.A.y)) / self.scale()

## Checks if two Quads fit into threshold
#  @param fst First Quad to compare.
#  @param snd Second Quad to compare.
#  @threshold thresold of check.
def compareQuadsByHash(fst, snd, threshold):
    return math.abs (fst.xC - snd.xC) < threshold and \
            math.abs (fst.yC - snd.yC) < threshold and\
            math.abs (fst.xD - snd.xD) < threshold and\
            math.abs (fst.yD - snd.yD) < threshold
