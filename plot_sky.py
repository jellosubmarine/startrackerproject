# Needs to import createDBIndex

import matplotlib.pyplot as plt
from createDBIndex import *
import healpy as hp
import numpy as np
import math

NSIDE = nside # Same as in createDBIndex.py

print "The pixel side is approximately", 360.0/NSIDE/4*2**0.5/2

bscMap = readCSV()
naive = filterCatalog(bscMap)
_, astrometry5 = filterAstrometry(bscMap,5)
_, astrometry3 = filterAstrometry(bscMap,3)

print "stars according to naive filter:", len(naive)
print "stars according to astrometry filter (5 brightest):", len(astrometry5)
print "stars according to astrometry filter (3 brightest):", len(astrometry3)

naive_num = np.zeros(hp.nside2npix(NSIDE))
for pixel in [ hp.pixelfunc.ang2pix(NSIDE, math.pi/2-s.dec, s.ra) for s in naive ]:
    naive_num[pixel] += 1

astrometry5_num = np.zeros(hp.nside2npix(NSIDE))
for pixel in [ hp.pixelfunc.ang2pix(NSIDE, math.pi/2-s.dec, s.ra) for s in astrometry5 ]:
    astrometry5_num[pixel] += 1

astrometry3_num = np.zeros(hp.nside2npix(NSIDE))
for pixel in [ hp.pixelfunc.ang2pix(NSIDE, math.pi/2-s.dec, s.ra) for s in astrometry3 ]:
    astrometry3_num[pixel] += 1

cutout5 = np.array([ naive_num[i]-astrometry5_num[i] for i in range(hp.nside2npix(NSIDE)) ])
lessthan4 = np.array([ naive_num[i] < 4 for i in range(hp.nside2npix(NSIDE)) ])
lessthan3 = np.array([ naive_num[i] < 3 for i in range(hp.nside2npix(NSIDE)) ])
zerostars = np.array([ naive_num[i] == 0 for i in range(hp.nside2npix(NSIDE)) ])

hp.mollview(naive_num,title="Stars per pixel")
hp.mollview(astrometry5_num,title="Stars per pixel after filterAstrometry (5)")
hp.mollview(cutout5,title="Stars that were eliminated with filterAstrometry (5)")
hp.mollview(lessthan4,title="Pixels with less than 4 stars")
hp.mollview(lessthan3,title="Pixels with less than 3 stars")
hp.mollview(zerostars,title="Pixels with 0 stars")

plt.show()
