#!/usr/bin/python

from astropy.io import fits
import sys
import math


# FITS init
table = fits.open(sys.argv[1], format='ascii')

print(table[1].header)
print(table[1].columns)

print "Number of rows: " + str(len(table[1].data))
print table[1].data;
