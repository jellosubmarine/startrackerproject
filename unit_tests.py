import unittest
from quad import *

class QuadTestCase(unittest.TestCase):
    """Tests for `quad.py`."""

    def test_rotate_around_X(self):
        self.assertAlmostEqual(0.1, 0.0)

    def test_rotate_around_Y(self):
        self.assertAlmostEqual(0.1, 0.0)

    def test_un_rotated_square(self):
        """Unrotataed square quad"""
        A = Star(1, 0, 0, 0, 0, 0, 1.0)
        B = Star(2, 1, 1, 0, 0, 0, 1.0)
        C = Star(3, 0, 1, 0, 0, 0, 1.0)
        D = Star(4, 1, 0, 0, 0, 0, 1.0)
        q = Quad([A, B, C, D])
        hash = q.getHash()
        for i in range(len(hash)):
            self.assertAlmostEqual(hash[i], [0.0,1.0,1.0,0.0][i])

    def test_rotation_sin_is_0(self):
        """Unrotataed square quad"""
        A = Star(1, 0, 0, 0, 0, 0, 1.0)
        B = Star(2, 1, 1, 0, 0, 0, 1.0)
        C = Star(3, 1, 0, 0, 0, 0, 1.0)
        D = Star(4, 0, 1, 0, 0, 0, 1.0)
        q = Quad([A, B, C, D])
        self.assertAlmostEqual(q.rotationSinA(), 0.0)

    def test_rotation_cos_is_1(self):
        """Unrotataed square quad"""
        A = Star(1, 0, 0, 0, 0, 0, 1.0)
        B = Star(2, 1, 1, 0, 0, 0, 1.0)
        C = Star(3, 1, 0, 0, 0, 0, 1.0)
        D = Star(4, 0, 1, 0, 0, 0, 1.0)
        q = Quad([A, B, C, D])
        self.assertAlmostEqual(q.rotationCosA(), 1.0)

if __name__ == '__main__':
    unittest.main()
