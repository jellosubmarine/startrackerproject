import numpy as np
import cv2
import sys

img = np.zeros((512,512,3), np.uint8)

xC, yC, xD, yD = float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3]), float(sys.argv[4])
cv2.circle(img,(int(xC*512),int(512-yC*512)), 3, (0,0,255), -1)
cv2.circle(img,(int(xD*512),int(512-yD*512)), 5, (0,0,255), -1)

print xC, yC, xD, yD

cv2.imshow("",img)

while 1:
    key = cv2.waitKey(1)
    if key == 27:
        break
