import itertools
from random import randint
from createDBIndex import *
from quad import *

def order(q):
    return [q.A.id, q.B.id, q.C.id, q.D.id]

bscMap = readCSV()

for i in range(300):
    stars = bscMap[randint(0,len(bscMap)-1)], bscMap[randint(0,len(bscMap)-1)], bscMap[randint(0,len(bscMap)-1)], bscMap[randint(0,len(bscMap)-1)]
    correctorder = order(Quad(copy.deepcopy(stars)))
    for p in itertools.permutations(stars):
        neworder = order(Quad(copy.deepcopy(p)))
        if not (correctorder[0] == neworder[0] and correctorder[1] == neworder[1] and correctorder[2] == neworder[2] and correctorder[3] == neworder[3]):
            print "Did not get the same rotation"
            print "Original stars:", stars
            print "Permutation that got a different result:", p
            raw_input()
